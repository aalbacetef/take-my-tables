/*
  Test the main parts of functionality associated with to-json parsing.
*/

const path = require('path');
const p = (...args) => path.join(...args);

//  module imports
const srcDir = path.resolve( p( __dirname, '..', 'src' ) );
const modesDir = p(srcDir, 'modes');

const toJson = require(p(modesDir, 'to-json'));
const test = require('tape');

test('testing to-json functionality', function(t) {

  t.comment('test CSV loading');
  /* @TODO implement this test! */


});
