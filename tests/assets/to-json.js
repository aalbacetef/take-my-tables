module.exports = {
  "fields": {
    "input": [
      "name",
      "brand",
      "price",
      "img"
    ],

    "output": [
      "name",
      "brand",
      "description",
      "price",
      "imgFile",
      "datetime"
    ]
  },

  "transformFunctions": {
    name(row) {
      return "Product - " + (row.name.length + row.brand.length);
    },
    brand(row) {
      return "Brand - " + row.brand;
    },
    description(row) { return "To be defined"; },
    price(row) { return parseFloat(row.price); },
    imgFile(row) {
      /*
        @TODO
          - download row.img (url)
          - return base64 string
      */
      return null;
    },
    datetime(row) {
      return Date.now();
    }
  },

  "types": {
    "input": {
      "name": "string",
      "brand"(inputVal) {
        return (typeof inputVal === 'string' && inputVal.length > 0);
      }
    }
  }
}
