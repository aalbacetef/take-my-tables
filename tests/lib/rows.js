const path = require('path');
const p = (...args) => path.join(...args);

//  module imports
const srcDir = path.resolve( p( __dirname, '..', 'src' ) );
const libDir = p(srcDir, 'lib');

const { isValidRow, transformRows, loadRows } = require(p(libDir, 'rows'));
const test = require('tape');

// mock data
const mockData = {
  'validRow': {
    'row': [ "arturo", 23, 0.99],
    'badRow': [ "arturo", 23 ],
    'fields': ['name', 'quantity', 'price'],
    'types': [ /* functionality not implemented yet */ ]
  },

  'loadRows': {
    'sourcePath': '',
    'badFilePath': '',
    'fields': [],
    'skipBadRows': false
  },

  'transformRows': {

  }
};

test('test group for rows.js', function(t) {
  t.plan(2);

  // rows.js: isValidRow
  t.comment('testing rows.js: isValidRow');
  const {badRow, row, fields, types} = mockData['validRow'];
  t.ok(
    isValidRow(row, fields, types),
    'this should return that the row is valid'
  );
  t.notOk(
    isValidRow(badRow, fields, types),
    'this should return that the row is invalid'
  );

  // rows.js: loadRows
  /* @TODO implement test for loadRows */


  // rows.js: transformRows
  /* @TODO implement test for transformRows */

});
