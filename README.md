# take-my-tables 


CLI utility for handling and managing CSV utilities in a simple fashion.

Usage:

almost all commands will make use of an output path, so specify this with:
    --out=output

* tabular CSV data to json (--to-json ):

    take-my-tables --to-json --out=output --csv=csv --manifest=manifest

    (for more information: take-my-tables --to-json help)

* run calculations over CSV data (--calc):

    take-my-tables --calc --out=output --csv=csv --manifest=manifest

    (for more information: take-my-tables --calc help)

