
demo_path=$(dirname $(realpath $0))
binary_path="$(dirname $demo_path)/src/main.js"
config_path="$demo_path/to-json.js"
source_path="$demo_path/base.csv"
dest_path="$demo_path/output.json"

take-my-tables --to-json \
  --out="$dest_path" \
  --csv="$source_path" \
  --manifest="$config_path"
