module.exports = {
  "fields": {
    "input": [
      "name",
      "brand",
      "price",
      "img"
    ],

    "output": [
      "name",
      "brand",
      "description",
      "price",
      "imgFile"
    ]
  },

  "transformFunctions": {
    name(row) {
      return "Product - " + row.name;
    },
    brand(row) {
      return "Brand - " + row.brand;
    },
    description(row) { return "To be defined"; },
    price(row) { return parseFloat(row.price); },
    imgFile(row) {
      /*
        @TODO
          - download row.img (url)
          - return base64 string
      */
      return null;
    }
  },

  "types": {
    "input": {
      "name": "string",
      "brand"(inputVal) {
        return (typeof inputVal === 'string' && inputVal.length > 0);
      }
    }
  }
}
