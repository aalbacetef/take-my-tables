module.exports = (function(){
  return { loadConfig, isUndef };

  function isUndef(...args) {
    return args.some( e => typeof e === 'undefined' );
  }

  function loadConfig(configPath) {
    const config = require(configPath);
    const fields = config.fields;
    const transformFunctions = config.transformFunctions;
    const fns = Object.keys(transformFunctions);
    const validFn = fns.every( fn => fields.output.indexOf(fn) > -1 );


    if( !validFn ) {
      throw {
        msg: new Error('transformFunctions does not match input field'),
        fields: fields,
        fns: fns
      };
    }

    return config;
  }


})();
