const fs = require('fs');

module.exports = (function(){

  return { isValidRow, transformRows, loadRows };

  /*** MODULE DEFINITION ***/

  /*
    @desc Performs a series of checks on a row, with the objective of only keeping
    rows that contain healthy data.
    Checks currently for the row having the correct number of fields.

    @TODO
      - implement optional type-checking
      - can be applied on a field basis, with all default to type: "any"

  */
  function isValidRow(row, fields, types) {
    const fieldMatch = (row.length === fields.length);
    const typeCheck = (/* @TODO implement*/ true );

    const validRow = fieldMatch && typeCheck;

    return validRow;
  }


  /*
    @desc Loads the file, runs basic validation on the data and returns this data
    ready to be parsed and transformed.

    @TODO
    - improve error message being thrown.
  */
  function loadRows(sourcePath, fields, skipBadRows) {
    let data = fs
      .readFileSync(sourcePath, 'utf8')
      .split('\n')
      .map( line => line.trim() )
      .filter( line => line.length > 0 )
      .map( r => r.split(',') )

    const skipFlag = ( typeof skipBadRows === 'undefined' )
      ? false
      : skipBadRows;

    if ( skipFlag !== true ) {
      const healthyData = data.every( row => isValidRow(row, fields) );
      if ( !healthyData ) { throw new Error('bad data'); }
    }

    // transform flat row array into a row object
    return data.map( row => {
      let o = {};
      let indx = 0;
      for(const field of fields) {
        o[field] = row[indx];
        indx++;
      }
      return o;
    })

  }

  /*
    @TODO add documentation
  */
  function transformRows(config) {
    return function(row) {
      const fields = config.fields;

      if ( row === null ) {
        return null;
      }

      let o = {};
      let indx;
      for( const field of config.fields.output ) {
        if( typeof config.transformFunctions[field] !== 'function' ) {
          continue;
        }

        let parsedValue = config.transformFunctions[field](row, config)
        o[field] = parsedValue;

      }

      return o;
    }
  }

})();
