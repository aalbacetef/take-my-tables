
const fs = require('fs');
const path = require('path');

const p = (...args) => path.join(...args);

const libDir = path.resolve(p(__dirname, '..', 'lib'));

const { isValidRow, transformRows, loadRows } = require(p(libDir, 'rows'));
const { isUndef, loadConfig } = require(p(libDir, 'utils'));


// script core
module.exports = function toJson(configPath, sourcePath, destPath) {

  // checks
  if( isUndef(configPath, sourcePath, destPath) ) {
    throw {
      msg: new Error('Undefined CLI args'),
      args: {configPath, sourcePath, destPath}
    };
  }
  const config = loadConfig(configPath);
  const inputData = loadRows(sourcePath, config.fields.input);
  const outputData = inputData.map( transformRows(config) )

  fs.writeFileSync(
    destPath,
    JSON.stringify(outputData, null, 2),
    'utf8'
  );

};
