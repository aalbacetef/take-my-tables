const colors = {
	//--- text colors ---
	//grayscale
	'white': ['\x1B[37m', '\x1B[39m'],
	'grey': ['\x1B[90m', '\x1B[39m'],
	'black': ['\x1B[30m', '\x1B[39m'],
	//others
	'blue': ['\x1B[34m', '\x1B[39m'],
	'cyan': ['\x1B[36m', '\x1B[39m'],
	'green': ['\x1B[32m', '\x1B[39m'],
	'magenta': ['\x1B[35m', '\x1B[39m'],
	'red': ['\x1B[31m', '\x1B[39m'],
	'yellow': ['\x1B[33m', '\x1B[39m'],
	//--- background colors ---
	//grayscale
	'whiteBG': ['\x1B[47m', '\x1B[49m'],
	'greyBG': ['\x1B[49;5;8m', '\x1B[49m'],
	'blackBG': ['\x1B[40m', '\x1B[49m'],
	//others
	'blueBG': ['\x1B[44m', '\x1B[49m'],
	'cyanBG': ['\x1B[46m', '\x1B[49m'],
	'greenBG': ['\x1B[42m', '\x1B[49m'],
	'magentaBG': ['\x1B[45m', '\x1B[49m'],
	'redBG': ['\x1B[41m', '\x1B[49m'],
	'yellowBG': ['\x1B[43m', '\x1B[49m']
};

const toJson = require(__dirname + '/modes/to-json');

module.exports = (function() {

  function it( text ) { return "\x1B[3m" + text + "\x1B[23m"; }
  function c(color, text) {
    const [h, t] = colors[color];
    return `${h}${text}${t}`;
  }
  function r(text) { return c('red', text); }
  function g(text) { return c('green', text); }
  function b(text) { return c('blue', text); }

  const modes = {
    'to-json':  toJsonWrapper,
    'j':        toJsonWrapper,
    'calc':     calcWrapper,
    'c':        calcWrapper
  };
  const promptText = `

    ${g("take-my-tables")} |
    ----------------

      CLI utility for handling and managing CSV utilities in a simple fashion.

      ${g("Usage")}:

        almost all commands will make use of an output path, so specify this with:
            --out=${it("output")}

        * ${r('tabular CSV data to json (--to-json )')}:

            take-my-tables --to-json --out=${it("output")} --csv=${it("csv")} --manifest=${it("manifest")}

            (for more information: take-my-tables --to-json help)

        * ${r('run calculations over CSV data (--calc):')}

            take-my-tables --calc --out=${it("output")} --csv=${it("csv")} --manifest=${"manifest"}

            (for more information: take-my-tables --calc help)
  `;

  function toJsonWrapper(outputPath, params) {
    const configPath = params['manifest'];
    const sourcePath = params['csv'];

    return toJson(configPath, sourcePath, outputPath);
  }
  function calcWrapper(outputPath, params){
    

  }


  return { modes, promptText };
})();
